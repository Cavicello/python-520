#!/usr/bin/python3
'''
calcular media de um aluno
leia duas notas
calcule a media
se a media >= 7 exibir aprovado
se a media < 7 e > 3 exibir recuperacao
se a media <= 3 exibir reprovado
'''

n1 = float( input( 'Digite a primeira nota:' ) )
n2 = float( input( 'Digite a segunda nota:'  ) )

#n1 = int( n1 )
#n2 = int( n2 )
media = float( ( n1+n2 ) / 2 )

print( 'nota1: ' , n1 )
print( 'nota2: ' , n2 )
print( 'media: ' , ( n1+n2 ) / 2 , end = '\n' )

if media >= 7:
   print( '>> Aprovado' , end = '\n' )
elif media > 3: #and media < 7:
   print( '>> Recuperacao' )   
else: 
   print( '>> Reprovado' )
